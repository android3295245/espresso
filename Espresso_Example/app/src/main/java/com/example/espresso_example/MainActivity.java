package com.example.espresso_example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//          INFO about espresso in link: https://www.youtube.com/watch?v=KZeU3tOKHp0&t=828s


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        TextView textView;
        Button b_reset, b_activity, b_cambiar;
        EditText editText;

        textView = (TextView) findViewById(R.id.textView);
        b_reset = (Button) findViewById(R.id.reset);
        b_activity = (Button) findViewById(R.id.cambiarActivity);
        b_cambiar = (Button) findViewById(R.id.cambiarTextView);
        editText = (EditText) findViewById(R.id.texto);

        b_reset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                editText.setText("Texto");
            }
        });

        b_activity.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                intent.putExtra("input", editText.getText().toString());
                startActivity(intent);
            }
        });

        b_cambiar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                textView.setText(editText.getText().toString());
            }
        });
    }
}