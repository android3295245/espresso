package com.example.espresso_example;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

//          INFO about espresso in link: https://www.youtube.com/watch?v=KZeU3tOKHp0&t=828s


public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        TextView texto2;
        Bundle inputData = getIntent().getExtras();
        String input = inputData.getString("input");

        texto2 = (TextView) findViewById(R.id.texto2);

        texto2.setText(input);
    }
}