package com.example.espresso_example;

import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;



//          INFO about espresso in link: https://www.youtube.com/watch?v=KZeU3tOKHp0&t=828s



@RunWith(AndroidJUnit4.class)
public class MainActivityEspressoTest
{
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void ensureTextChangesWork()
    {
        onView(withId(R.id.texto)).perform(typeText("HOLA"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.cambiarTextView)).perform(click());

        onView(withId(R.id.texto)).check(matches(withText("HOLA")));
    }

    @Test
    public void ensureResetWork()
    {
        onView(withId(R.id.texto)).perform(typeText("HOLA"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.reset)).perform(click());

        onView(withId(R.id.texto)).check(matches(withText("Texto")));           //Can change the string and the test will fail, confirming its functionality.
    }

    @Test
    public void changeNewActivity()
    {
        onView(withId(R.id.texto)).perform(typeText("Activity dos"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.cambiarActivity)).perform(click());

        onView(withId(R.id.texto2)).check(matches(withText("Activity dos")));
    }
}

